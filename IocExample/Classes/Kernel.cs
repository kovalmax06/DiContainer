﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace IocExample
{
    class Kernel
    {
        private readonly Dictionary<Type, Func<object>> dictionaryType = new Dictionary<Type, Func<Object>>();
        public void Bind<TKey, TConcrete>() 
        {
            dictionaryType[typeof(TKey)] = () => ResolveByType(typeof(TConcrete));
        }

        private object ResolveByType(Type type)
        { 
            var constructor = Utils.GetSingleConstructor(type);

            if (constructor != null)
            {
                var arguments = constructor.GetParameters()
                                           .Select(p => Resolve(p.ParameterType))
                                           .ToArray();

                return constructor.Invoke(arguments);
            }
            return null;
        }

        private object Resolve(Type parameterType)
        {
            Func<object> provider;

            if (dictionaryType.TryGetValue(parameterType, out provider))
            {
                return provider();
            }
            return Utils.CreateInstance(parameterType);
                
        }

        internal TKey Resolve<TKey>()
        {
            return (TKey)Resolve(typeof(TKey));
        }

        public void Bind<T>(T instance)
        {
            dictionaryType[typeof(T)] = () => instance;
        }
       
    }
}
