﻿using IocExample.Classes;
using Ninject;
using System;

namespace IocExample
{
    class Program
    {
        //static void Main(string[] args)
        //{
        //    var logger = new ConsoleLogger();
        //    var sqlConnectionFactory = new SqlConnectionFactory("SQL Connection", logger);
        //    var createUserHandler = new CreateUserHandler(new UserService(new QueryExecutor(sqlConnectionFactory), new CommandExecutor(sqlConnectionFactory), new CacheService(logger, new RestClient("API KEY"))), logger);

        //    createUserHandler.Handle();
        //}

        //static void Main(string[] args)
        //{
        //    IKernel kernel = new StandardKernel();

        //    kernel.Bind<ILogger>().To<ConsoleLogger>();
        //    kernel.Bind<UserService>().To<UserService>();
        //    kernel.Bind<QueryExecutor>().To<QueryExecutor>();
        //    kernel.Bind<CommandExecutor>().To<CommandExecutor>();
        //    kernel.Bind<CacheService>().To<CacheService>();
        //    kernel.Bind<RestClient>()
        //        .ToConstructor(k => new RestClient("API_KEY"));
        //    kernel.Bind<CreateUserHandler>().To<CreateUserHandler>();
        //    kernel.Bind<IConnectionFactory>()
        //        .ToConstructor(k => new SqlConnectionFactory("SQL Connection", k.Inject<ILogger>()))
        //        .InSingletonScope();

        //    var createUserHandler = kernel.Get<CreateUserHandler>();

        //    createUserHandler.Handle();
        //    Console.ReadKey();
           
        //}

        static void Main(string[] args)
        {

              var kernel = new Kernel();

             kernel.Bind<ILogger, ConsoleLogger>();
             kernel.Bind<UserService, UserService>();
             kernel.Bind<QueryExecutor, QueryExecutor>();
             kernel.Bind<CommandExecutor, CommandExecutor>();
             kernel.Bind<CacheService, CacheService>();
             kernel.Bind(new RestClient("API_KEY"));
             kernel.Bind<CreateUserHandler, CreateUserHandler>();
             kernel.Bind<IConnectionFactory>(new SqlConnectionFactory("SQL Connection", kernel.Resolve<ILogger>()));
             

            var createUserHandler = kernel.Resolve<CreateUserHandler>();
            createUserHandler.Handle();
            Console.ReadKey();

        }
    }
}
